# .env to Azure environment config

Converts .env files into Azure environment configuration files

## Usage
```text
Usage:
  dotenv-to-azure-env

Flags:
  -h, --help      Show help text
  -f, --file      Files to convert to single output file
  -o, --out-file  Filepath to output file. If not specify will print json to stdout
```

## Example

Example command:
```shell script
dotenv-to-azure-env -f hello.env -f yellow.env
```

Example input:

`hello.env`
```dotenv
HELLO=world
```

`yello.env`
```dotenv
YELLOW=bellow
```

Example output
```json
[
    {
        "name": "HELLO",
        "value": "world",
        "slotSetting": false
    },
    {
        "name": "YELLOW",
        "value": "bellow",
        "slotSetting": false
    }
]
```

