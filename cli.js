#!/usr/bin/env node

const dotenv = require('dotenv');
const commandLineArgs = require('command-line-args');
const fs = require('fs');
const chalk = require('chalk');

const optionsDefinitions = [
    {
        name: 'help', alias: 'h', type: Boolean, default: false, description: 'Show help text',
    },
    {
        name: 'file', alias: 'f', type: String, multiple: true, description: 'Files to convert to single output file',
    },
    {
        name: 'out-file', alias: 'o', type: String, multiple: false, description: 'Filepath to output file. If not specify will print json to stdout'
    }
]

const options = commandLineArgs(optionsDefinitions);

// noinspection JSUnresolvedVariable
if(options.help){
    console.log('Usage:');
    console.log('  dotenv-to-azure-env\n');
    console.log('Flags:');

    let maxNameLength = 0;
    for(const definition of optionsDefinitions){
        if(definition.name.length > maxNameLength){
            maxNameLength = definition.name.length;
        }
    }

    for(const definition of optionsDefinitions){
        const alias = definition.alias ? (`-${definition.alias}`).padStart(2, ' ') : '  ';
        const aliasSection = definition.alias ? `  ${alias},` : ''.padStart(5, ' ');
        const name = definition.name.padEnd(maxNameLength, ' ');
        console.log(`${aliasSection} --${name}  ${definition.description}`)
    }
    process.exit(0);
}

// noinspection JSUnresolvedVariable
if(!options.file || options.file.length === 0){
    console.log(chalk.red('Must specify at least 1 input file via -f or --file'));
    process.exit(1);
}


/**
 * @returns {Promise<void>}
 */
const run = async () => {

    let configs = [];
    // noinspection JSUnresolvedVariable
    for(const file of options.file){
        configs = configs.concat(await getFileConfigs(file))
    }
    if(options['out-file']){
        await convertFile(configs);
    }else{
        console.log(JSON.stringify(configs, null, 4));
    }
}

/**
 * @param {string} file
 * @returns {Promise<string>}
 */
const readFile = async (file) => {
    return new Promise((resolve, reject)=> {
        fs.readFile(file, (err, data) => {
            if(!err){
                resolve(data.toString());
            }else{
                reject(chalk.red(err));
            }
        })
    })
}

/**
 * @param configs
 * @returns {Promise<void>}
 */
const convertFile = async (configs) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(options['out-file'], JSON.stringify(configs, null, 4), (err, data) => {
            if(!err){
                resolve();
            }else{
                reject(err);
            }
        });
    })
}

const getFileConfigs = async (file) => {
    const fileContents = await readFile(file);
    const config = dotenv.parse(fileContents);

    const configItems = [];
    // noinspection JSUnfilteredForInLoop
    for(const key in config) {
        configItems.push({
            name: key,
            value: config[key],
            slotSetting: false
        })
    }
    return configItems;
}

run().then(() => {
    console.log('Finished converting');
})